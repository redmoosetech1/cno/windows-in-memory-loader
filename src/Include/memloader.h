#ifndef _MEMLOADER_H
#define _MEMLOADER_H
#include <minwindef.h>

typedef struct _loader LOADER, *PLOADER;
typedef PLOADER LHANDLE;

__declspec(dllexport) LHANDLE __cdecl Loader_Init(LPVOID lpOriginalImage);
__declspec(dllexport) DWORD __cdecl Loader_LoadImage(LHANDLE Loader);
__declspec(dllexport) DWORD __cdecl Loader_UnLoadImage(LHANDLE Loader);
__declspec(dllexport) DWORD __cdecl Loader_Fini(LHANDLE Loader);
__declspec(dllexport) LPVOID __cdecl Loader_GetProcAddress(LHANDLE Loader, PCHAR szFuncName);

#endif
