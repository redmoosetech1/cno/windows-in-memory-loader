#ifndef _LOADLIB_H
#define _LOADLIB_H
#include <minwindef.h>

#define CalculateOffset(x, y) ((UINT_PTR)x + y)
#define CalculateRelocDelta(x, y) (ptrdiff_t)((UINT_PTR)x - y)


typedef BOOL(WINAPI* DllEntryProc)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);
typedef int(WINAPI* WinMainEntryProc)(HINSTANCE hInst, HINSTANCE hInstPrev, PSTR cmdline, int cmdshow);
typedef int(WINAPI* MainEntryProc)(int argc, char* argv[], char* envp[]);

typedef struct _loader {
	LPVOID lpDstModuleBase;
	LPVOID lpSrcModuleBase;
	LPVOID lpPreferredImageBase;
	DWORD lpEntryPointRVA;
	DWORD dwSizeofHeaders;
	DWORD dwSizeOfImage;
	WORD ImageType;
	PIMAGE_NT_HEADERS NTHeader;
	IMAGE_OPTIONAL_HEADER OptionalHeader;
	IMAGE_FILE_HEADER FileHeader;
}LOADER, *PLOADER;

VOID FillHandleStructure(PLOADER pLoader, LPVOID lpOriginalImage);
DWORD HandleAllocations(PLOADER Loader);
VOID CopySectionHeaders(PLOADER Loader);
DWORD HandleRelocations(PLOADER Loader);
DWORD BaseRelocationFixup(PLOADER Loader);
DWORD ImportFixup(PLOADER Loader);
VOID ChangeProtections(PLOADER Loader);
DWORD HandleStartup(PLOADER Loader);
DWORD HandleTLSCallBacks(PLOADER Loader);
DWORD CallEntrypoint(PLOADER Loader);
DWORD StartExe(PLOADER Loader);
DWORD StartDll(PLOADER Loader);
DWORD StopDll(PLOADER Loader);

#endif