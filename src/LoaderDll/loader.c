#include <Windows.h>
#include <winternl.h>
#include <stdio.h>
#include <loadlib.h>
#include <memloader.h>


__declspec(dllexport) LHANDLE __cdecl Loader_Init(LPVOID lpOriginalImage)
{
	PLOADER LHandle = NULL;
	PLOADER pLoader = NULL;
	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)lpOriginalImage;
	PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)CalculateOffset(lpOriginalImage, pDosHeader->e_lfanew);

	// Check MZ Header
	if ((pDosHeader->e_magic) != IMAGE_DOS_SIGNATURE)
	{
		printf("[!] Image does not have MZ ...");
		goto cleanup;
	}

	// Check PE SIG
	if ((pNTHeaders->Signature) != IMAGE_NT_SIGNATURE)
	{
		printf("[!] Image does not have PE ...");
		goto cleanup;
	}

	// Check Optional Header
	if (pNTHeaders->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC)
	{
		printf("[!] Image does not have Optional Header, this may be a COFF Format ...\n");
		goto cleanup;
	}

	pLoader = HeapAlloc(GetProcessHeap(), 0, sizeof(LOADER));
	if (NULL == pLoader)
	{
		printf("[!] Failed to Allocate Memory for LOADER Data Structure ...\n");
		goto cleanup;
	}

	FillHandleStructure(pLoader, lpOriginalImage);
	LHandle = pLoader;
cleanup:
	return LHandle;
}


__declspec(dllexport) DWORD __cdecl Loader_LoadImage(LHANDLE Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	
	dwError = HandleAllocations(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	CopySectionHeaders(Loader);

	dwError = HandleRelocations(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	ChangeProtections(Loader);
	
	dwError = HandleStartup(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

__declspec(dllexport) DWORD __cdecl Loader_UnLoadImage(LHANDLE Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	DWORD oldProtect = 0;

	dwError = StopDll(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	if (NULL != Loader)
	{
		if (VirtualProtect(Loader->lpDstModuleBase, Loader->dwSizeOfImage, PAGE_READWRITE, &oldProtect))
		{
			VirtualFree((LPVOID)Loader->lpDstModuleBase, Loader->dwSizeOfImage, MEM_RELEASE);
			ZeroMemory(Loader->lpDstModuleBase, Loader->dwSizeOfImage);
			Loader->lpDstModuleBase = NULL;
			Loader->lpSrcModuleBase = NULL;
		}
	}

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

__declspec(dllexport) DWORD __cdecl Loader_Fini(LHANDLE Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;

	if (NULL == Loader)
	{
		printf("[!] Can't Free NULL LHandle ...\n");
		goto cleanup;
	}

	ZeroMemory(Loader, sizeof(Loader));
	HeapFree(GetProcessHeap(), 0, Loader);
	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

__declspec(dllexport) LPVOID __cdecl Loader_GetProcAddress(LHANDLE Loader, PCHAR szFuncName)
{
	LPVOID lpFuncProc = NULL;
	PIMAGE_EXPORT_DIRECTORY pExportDirectory = (PIMAGE_EXPORT_DIRECTORY)CalculateOffset(Loader->lpDstModuleBase, Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
	PDWORD pExportAddressTable = (PDWORD)CalculateOffset(Loader->lpDstModuleBase, pExportDirectory->AddressOfFunctions);
	PDWORD pExportNameTable = (PDWORD)CalculateOffset(Loader->lpDstModuleBase, pExportDirectory->AddressOfNames);
	
	if (Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress == 0)
	{
		printf("[!] No Export Directory Found ...\n");
		goto cleanup;
	}

	for (DWORD i = 0;  i < pExportDirectory->NumberOfFunctions; i++, pExportNameTable++, pExportAddressTable++)
	{
		PCHAR pExportName = (PCHAR)CalculateOffset(Loader->lpDstModuleBase, *pExportNameTable);
		if (strcmp(pExportName, szFuncName) == 0)
		{
			lpFuncProc = (LPVOID)CalculateOffset(Loader->lpDstModuleBase, *pExportAddressTable);
			break;
		}

		continue;
	}

cleanup:
	return lpFuncProc;
}

VOID FillHandleStructure(PLOADER pLoader, LPVOID lpOriginalImage)
{
	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)lpOriginalImage;
	
	// Fill in Headers
	pLoader->NTHeader =(PIMAGE_NT_HEADERS)CalculateOffset(lpOriginalImage, pDosHeader->e_lfanew);
	pLoader->OptionalHeader = pLoader->NTHeader->OptionalHeader;
	pLoader->FileHeader = pLoader->NTHeader->FileHeader;
	
	// Fill in Attributes needed
	pLoader->lpSrcModuleBase = lpOriginalImage;
	pLoader->lpPreferredImageBase = (LPVOID)pLoader->OptionalHeader.ImageBase;
	pLoader->lpEntryPointRVA = pLoader->OptionalHeader.AddressOfEntryPoint;
	pLoader->dwSizeofHeaders = pLoader->OptionalHeader.SizeOfHeaders;
	pLoader->dwSizeOfImage = pLoader->OptionalHeader.SizeOfImage;
	pLoader->ImageType = pLoader->FileHeader.Characteristics;
}

DWORD HandleAllocations(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	
	// Try to Allocate Memory at Preffered Image Base
	Loader->lpDstModuleBase = VirtualAlloc(Loader->lpPreferredImageBase, 
			Loader->dwSizeOfImage, 
			(MEM_RESERVE | MEM_COMMIT), 
			PAGE_EXECUTE_READWRITE);
	if (Loader->lpDstModuleBase == NULL)
	{
		// If ImageBase Collision then
		// Allocate memory where we can
		Loader->lpDstModuleBase = VirtualAlloc(NULL, 
			Loader->dwSizeOfImage, 
			(MEM_RESERVE | MEM_COMMIT), 
			PAGE_EXECUTE_READWRITE);
		if (Loader->lpDstModuleBase == NULL)
		{
			int dwError = ERROR_GEN_FAILURE;
			goto cleanup;
		}
	}
	ZeroMemory(Loader->lpDstModuleBase, Loader->dwSizeOfImage);
	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

VOID CopySectionHeaders(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	PIMAGE_SECTION_HEADER pSectionHeaders = IMAGE_FIRST_SECTION(Loader->NTHeader);
	WORD wNumOfSections = Loader->FileHeader.NumberOfSections;
	
	// Copy Headers to new Module Address Space
	CopyMemory(Loader->lpDstModuleBase, Loader->lpSrcModuleBase, Loader->dwSizeofHeaders);
	
	for (WORD wCount = 0; wCount < wNumOfSections; wCount++, pSectionHeaders++)
	{
		// Get VA for Section
		LPVOID uiDstVA = (LPVOID)CalculateOffset(Loader->lpDstModuleBase, pSectionHeaders->VirtualAddress);
		// Get Offset of Section from old Image
		LPVOID uiSrcVA = (LPVOID)CalculateOffset(Loader->lpSrcModuleBase, pSectionHeaders->PointerToRawData);

		// Copy Sections from OldImage to new Module Address Space
		if (pSectionHeaders->SizeOfRawData != 0)
			CopyMemory(uiDstVA, uiSrcVA, pSectionHeaders->SizeOfRawData);
		else
			ZeroMemory(uiDstVA, pSectionHeaders->Misc.VirtualSize);
	}
}

DWORD HandleRelocations(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	LPVOID lpImageBase = Loader->lpPreferredImageBase;

	dwError = ImportFixup(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	if (lpImageBase == Loader->lpDstModuleBase)
	{
		printf("[!] Already loaded a preffered Image Base: %p\n", Loader->lpDstModuleBase);
		dwError = ERROR_SUCCESS;
		goto cleanup;
	}

	dwError = BaseRelocationFixup(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;
	
	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD BaseRelocationFixup(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE; 
	ptrdiff_t BaseRelocDelta = CalculateRelocDelta(Loader->lpDstModuleBase, Loader->OptionalHeader.ImageBase);

	if (Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress == 0)
	{
		printf("[!] Relocation Table does not Exist ...\n");
		goto cleanup;
	}

	if (BaseRelocDelta == 0)
	{
		printf("[!] Image Already Loaded at correct address ...\n");
		goto cleanup;
	}

	PIMAGE_BASE_RELOCATION BaseRelocationTable = (PIMAGE_BASE_RELOCATION)CalculateOffset(Loader->lpDstModuleBase, Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
	for (; BaseRelocationTable->VirtualAddress != 0; BaseRelocationTable = (PIMAGE_BASE_RELOCATION)CalculateOffset(BaseRelocationTable, BaseRelocationTable->SizeOfBlock))
	{
		DWORD dwRelocBlockSize = (BaseRelocationTable->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
		PWORD pwRelocation = (PWORD)(BaseRelocationTable + 1);
		for (DWORD i = 0; i < dwRelocBlockSize; i++, pwRelocation++)
		{
			WORD wRelocType = *pwRelocation >> 12;
			WORD wRelocOffset = *pwRelocation & 0xfff;

			PULONGLONG pllRelocAddress = (PULONGLONG)(CalculateOffset(Loader->lpDstModuleBase, BaseRelocationTable->VirtualAddress) + wRelocOffset);
			// there is only one type used that needs to make a change
			switch (wRelocType) 
			{
				case IMAGE_REL_BASED_HIGHLOW:
					*pllRelocAddress += BaseRelocDelta;
					break;
				default:
					break;
			}
		}
	}
	
	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD ImportFixup(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)CalculateOffset(Loader->lpDstModuleBase, Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

	for (; pImportDescriptor->OriginalFirstThunk != 0; pImportDescriptor++)
	{
		// Get Name Of Module/DLL
		PCHAR lpModuleName = (PCHAR)CalculateOffset(Loader->lpDstModuleBase, pImportDescriptor->Name);
		HMODULE hmCurrentModule = LoadLibraryA(lpModuleName);
		if (hmCurrentModule == NULL)
		{
			printf("[!] Could not load module named: %s\n", lpModuleName);
			goto cleanup;
		}
		
		PIMAGE_THUNK_DATA pIDT = (PIMAGE_THUNK_DATA)CalculateOffset(Loader->lpDstModuleBase, pImportDescriptor->OriginalFirstThunk);
		PIMAGE_THUNK_DATA pIAT = (PIMAGE_THUNK_DATA)CalculateOffset(Loader->lpDstModuleBase, pImportDescriptor->FirstThunk);

		for (; pIDT->u1.AddressOfData != 0; pIDT++, pIAT++)
		{
			ULONGLONG ullFunctionVA = 0;
			ULONGLONG ullFunctionRVA = pIDT->u1.AddressOfData;

			if ((ullFunctionRVA & IMAGE_ORDINAL_FLAG) == 0)
			{
				PIMAGE_IMPORT_BY_NAME FunctionImportName = (PIMAGE_IMPORT_BY_NAME)CalculateOffset(Loader->lpDstModuleBase, ullFunctionRVA);
				ullFunctionVA = (ULONGLONG)GetProcAddress(hmCurrentModule, (PCHAR)&(FunctionImportName->Name));
			}
			else
				ullFunctionVA = (ULONGLONG)GetProcAddress(hmCurrentModule, (LPCSTR)ullFunctionRVA);

			if (ullFunctionVA == 0)
			{
				printf("[!] GetProcAddress Failed to get function ...\n");
				goto cleanup;
			}

			pIAT->u1.AddressOfData = ullFunctionVA;
		}
	}

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

VOID ChangeProtections(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	DWORD dwOldProtections = 0;
	PIMAGE_SECTION_HEADER pSectionHeaders = IMAGE_FIRST_SECTION(Loader->NTHeader);

	VirtualProtect(Loader->lpDstModuleBase, Loader->dwSizeOfImage, PAGE_READONLY, &dwOldProtections);

	for (int i = 0; i < Loader->FileHeader.NumberOfSections; i++, pSectionHeaders++)
	{
		// Get VA for Section
		LPVOID uiDstVA = (LPVOID)CalculateOffset(Loader->lpDstModuleBase, pSectionHeaders->VirtualAddress);
		DWORD dwSrcProtections = pSectionHeaders->Characteristics;
		DWORD dwDstProtections = 0;

		if (dwSrcProtections & IMAGE_SCN_MEM_EXECUTE)
			dwDstProtections = (dwSrcProtections & IMAGE_SCN_MEM_WRITE) ? PAGE_EXECUTE_READWRITE: PAGE_EXECUTE_READ;
		else
			dwDstProtections = (dwSrcProtections & IMAGE_SCN_MEM_WRITE) ? PAGE_READWRITE : PAGE_READONLY;

		VirtualProtect(uiDstVA, pSectionHeaders->Misc.PhysicalAddress, dwDstProtections, &dwOldProtections);
	}
}

DWORD HandleStartup(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	dwError = HandleTLSCallBacks(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	dwError = CallEntrypoint(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD HandleTLSCallBacks(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	PIMAGE_TLS_DIRECTORY pTLSDirectory = (PIMAGE_TLS_DIRECTORY)CalculateOffset(Loader->lpDstModuleBase, Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress);
	PULONGLONG pCallBacks = (PULONGLONG)CalculateOffset(pTLSDirectory, pTLSDirectory->AddressOfCallBacks);
	
	if (Loader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress == 0)
	{
		printf("[!] No TLSCallbacks ...\n");
		dwError = ERROR_SUCCESS;
		goto cleanup;
	}

	for (;pCallBacks != NULL; pCallBacks++)
	{
		PIMAGE_TLS_CALLBACK Callback = (PIMAGE_TLS_CALLBACK)pCallBacks;
		Callback((LPVOID)Loader->lpDstModuleBase, DLL_PROCESS_ATTACH, NULL);
	}
	
	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD CallEntrypoint(PLOADER Loader)
{
	DWORD dwIsDll = (Loader->ImageType & IMAGE_FILE_DLL);
	return (dwIsDll == IMAGE_FILE_DLL) ? StartDll(Loader) : StartExe(Loader);
}

DWORD StartExe(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	WORD wSubSystem = Loader->OptionalHeader.Subsystem;
	switch (wSubSystem)
	{
		case IMAGE_SUBSYSTEM_WINDOWS_GUI:
			break;
		case IMAGE_SUBSYSTEM_WINDOWS_CUI:
			break;
		default:
			goto cleanup;
	}

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD StartDll(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	BOOL bIsLoaded = FALSE;
	LPVOID DLLMain = (LPVOID)CalculateOffset(Loader->lpDstModuleBase, Loader->lpEntryPointRVA);
	DllEntryProc EntryPoint = (DllEntryProc)DLLMain;

	bIsLoaded = (*EntryPoint)((HINSTANCE)Loader->lpDstModuleBase, DLL_PROCESS_ATTACH, NULL);
	if (bIsLoaded == FALSE)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

DWORD StopDll(PLOADER Loader)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	BOOL bIsLoaded = FALSE;
	LPVOID DLLMain = (LPVOID)CalculateOffset(Loader->lpDstModuleBase, Loader->lpEntryPointRVA);
	DllEntryProc EntryPoint = (DllEntryProc)DLLMain;

	bIsLoaded = (*EntryPoint)((HINSTANCE)Loader->lpDstModuleBase, DLL_PROCESS_DETACH, NULL);
	if (bIsLoaded == FALSE)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	return dwError;
}

BOOL WINAPI DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID lpReserved)  // reserved
{
    // Perform actions based on the reason for calling.
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        // Initialize once for each new process.
        // Return FALSE to fail DLL load.
        break;

    case DLL_THREAD_ATTACH:
        // Do thread-specific initialization.
        break;

    case DLL_THREAD_DETACH:
        // Do thread-specific cleanup.
        break;

    case DLL_PROCESS_DETACH:
        // Perform any necessary cleanup.
        break;
    }
    return TRUE;  // Successful DLL_PROCESS_ATTACH.
}
