#include <Windows.h>
#include <winternl.h>
#include <stdio.h>
#include <memloader.h>

#pragma comment(lib, "LoaderDll.lib")

typedef VOID(_cdecl *HELLOWORLD)(VOID);
DWORD MapFile(const char* szFileName, PHANDLE phFileMapping, LPVOID* lpFileBase);

int main(int argc, char* argv[], char** envp)
{
	DWORD dwError = ERROR_GEN_FAILURE;
	LPVOID lpOriginalImageBase = NULL;
	HANDLE hFileMapping = NULL;
	LHANDLE Loader = NULL;
	HELLOWORLD HelloWorld = NULL;
	const char* szFileName = "C:\\Users\\Nate0\\Nextcloud\\Development\\InMemoryLoader\\build\\x64\\Release\\HelloWorld.dll";
	
	dwError = MapFile(szFileName, &hFileMapping, &lpOriginalImageBase);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	Loader = Loader_Init(lpOriginalImageBase);
	if (Loader == NULL)
		goto cleanup;

	dwError = Loader_LoadImage(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	HelloWorld = (HELLOWORLD)Loader_GetProcAddress(Loader, "HelloWorld");
	if (NULL == HelloWorld)
		goto cleanup;

	HelloWorld();

	dwError = Loader_UnLoadImage(Loader);
	if (dwError != ERROR_SUCCESS)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	if (hFileMapping != 0)
		UnmapViewOfFile(hFileMapping);
	Loader_Fini(Loader);
	return ERROR_SUCCESS;
}

DWORD MapFile(const char* szFileName, PHANDLE phFileMapping, LPVOID* lpOriginalImageBase)
{
	int iBytesRead = 0;
	int iFileSize = 0;
	DWORD dwError = ERROR_GEN_FAILURE;
	HANDLE hFile = INVALID_HANDLE_VALUE;

	hFile = CreateFile(szFileName, GENERIC_READ, 
		               (FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE), 
		               NULL, 
		               OPEN_EXISTING, 
		               FILE_ATTRIBUTE_NORMAL, 
		               NULL);
	if ((INVALID_HANDLE_VALUE == hFile) || (NULL == hFile))
	{
		printf("[!] Couldn't open file ...");
		goto cleanup;
	}

	*phFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (*phFileMapping == 0)
		goto cleanup;

	*lpOriginalImageBase = MapViewOfFile(*phFileMapping, FILE_MAP_READ, 0, 0, 0);
	if (*lpOriginalImageBase == 0)
		goto cleanup;

	dwError = ERROR_SUCCESS;
cleanup:
	if ((hFile != INVALID_HANDLE_VALUE) || (hFile != NULL))
		CloseHandle(hFile);
	return dwError;
}
